#!/bin/bash
#===================================================================
# ${COLOR_LIB}proxy-functions.sh${CRESET}
# Lib defining proxy and tunnel functions. For more type \"proxy\" or \"tunnel\"
#
# Author: Diego Schmidt
# Since: 2014-08-11
#===================================================================
source /opt/devtools/lib/utils.sh
source /opt/devtools/lib/window.sh

# ----------------------------------------------------------------------------
# CNTLM
# ----------------------------------------------------------------------------
#---
# ${COLOR_FUNC}proxy${CRESET}
# Functions to start, stop, restart and configure CNTL proxy
# Usage: ${COLOR_FUNC}proxy${CRESET} <argument>
# Arguments: --start   | start   | -s    Start
#            --stop    | stop    | -k    Stop
#            --restart | restart | -r    Restart
#            --config  | config  | -c    Configure
#
# Ex: proxy --start
#     proxy -s
#     proxy start
#
# Author: Diego Schmidt && Pedro Leao
# Since: 2014-08-11
#---
function proxy() {
	local OPTIND
	# Parametros longos, NAO MUDAR A ORDEM
	set -- ${@//--start/-s};
	set -- ${@//--stop/-k};
	set -- ${@//--restart/-r};
	set -- ${@//--config/-c};
	set -- ${@//restart/-r};
	set -- ${@//start/-s};
	set -- ${@//stop/-k};
	set -- ${@//config/-c};

	local VALID_OPT=
	while getopts ":skrc" opt ; do
		case $opt in
			s) 	_proxyStart;;
			k) 	tskill /A cntlm 2>/dev/null;
				successln "Proxy stopped!";;
			r) 	info "Restarting Proxy!";
				tskill /A cntlm 2>/dev/null;
				sleep 5;
				_proxyStart;;
			c) 	tskill /A cntlm 2>/dev/null;
				_proxyConfig
				_proxyStart;;
			\?) usage "${BASH_SOURCE[0]}" proxy; return 1;;
			*) usage "${BASH_SOURCE[0]}" proxy; return 1;;
		esac
		VALID_OPT="OK"
	done
	shift $(($OPTIND - 1))

	if [ -z ${VALID_OPT} ]; then
		usage "${BASH_SOURCE[0]}" proxy;
		return 1;
	fi
}

function _proxyStart() {
	local prog=cntlm;
	local opts="-c $(cygpath -m ${HOME}/.cntlmrc)";
	local status=;

	echo "${@}" | egrep -q -- "-c" && opts=;

	ps -Wef | grep -qw "${prog}" && {
		status="running";
	}  || {
		status="starting";
		${prog} ${PROXY_PARMS} ${@} ${opts};
	}
	successln "${prog} ${status}"
}

function _proxyConfig() {
	local user=;
	local domain=;
	local passwd=;
	local passwd1=;
	local passwd2=;
	local port=;
	local proxy=;
	local proxyhost=;
	local proxyport=;

	while [ -z "${user}" -o -z "${domain}" ] ; do
		user="${USERNAME}";
		domain="${USERDOMAIN}";

		user=$( showDialog \
			--stdout \
			--title " USERNAME " \
			--inputbox ":: Inform username" 10 30 ${domain}\\${user} || echo "__exit" )

		[ "${user}" == "__exit" ] && return;
		echo ${user} | egrep -q "[a-zA-Z]+.[a-zA-Z]+" && {
			domain=${user%%\\*}
			user=${user##*\\};
		} || user=;
	done

	while [ -z "${passwd}" ] ; do
		while [ -z "${passwd1}" ] ; do
			passwd1=$( showDialog \
				--stdout \
				--title " PASSWORD " \
				--passwordbox ":: Inform password" 10 30 ${passwd1} || echo "__exit" )
			[ "${passwd1}" == "__exit" ] && return;
		done

		while [ -z "${passwd2}" ] ; do
			passwd2=$( showDialog \
				--stdout \
				--title " PASSWORD " \
				--passwordbox ":: Confirm password" 10 30 ${passwd2} || echo "__exit" )
			[ "${passwd2}" == "__exit" ] && return;
		done

		if [[ "${passwd1}" == "${passwd2}" ]]; then
			passwd="${passwd1}";
		else
			passwd1=;
			passwd2=;
			showDialog --title " OOPS... "  \
				--msgbox "Passwords do not match. Try again." 10 30;
		fi
	done

	while [ -z "${port}" ] ; do
		port=31281;
		port=$( showDialog \
			--stdout \
			--title " LISTEN PORT " \
			--inputbox ":: Inform listen port" 10 30 ${port} || echo "__exit" )
		[ "${port}" == "__exit" ] && return;
	done

	while [ -z "${proxyhost}" ] ; do
		proxyhost="${PROXY_HOST}";
		if [ -z "${proxyhost}" ]; then
			proxyhost="ncproxy1";
		fi
		proxyhost=$( showDialog \
			--stdout \
			--title " PROXY HOST " \
			--inputbox ":: Inform proxy host" 10 30 ${proxyhost} || echo "__exit" )
		[ "${proxyhost}" == "__exit" ] && return;
	done

	while [ -z "${proxyport}" ] ; do
		proxyport="${PROXY_PORT}";
		if [ -z "${proxyport}" ]; then
			proxyport="8080";
		fi
		proxyport=$( showDialog \
			--stdout \
			--title " PROXY PORT " \
			--inputbox ":: Inform proxy port" 10 30 ${proxyport} || echo "__exit" )
		[ "${proxyport}" == "__exit" ] && return;
	done

	proxy="${proxyhost}:${proxyport}"

	( sleep 2; echo "${passwd}" ) | cntlm -u "${user}@${domain}" -I -H | tail -3 >~/.cntlmrc;
	cat >> ~/.cntlmrc <<EOF
NoProxy         127.1,127.0.0.1,localhost,${HOSTNAME},$( ipconfig | grep "IP \." | cut -d : -f 2 | tr -d ' ' )
Username        ${user}
Domain          ${domain}
Proxy           ${proxy}
Listen          0.0.0.0:${port}
EOF

}

# ----------------------------------------------------------------------------
# TUNNEL
# ----------------------------------------------------------------------------
#---
# ${COLOR_FUNC}tunnel${CRESET}
# Functions to start and kill a tunnel
# Usage: ${COLOR_FUNC}tunnel${CRESET} <argument>
# Arguments: -v <server> | --verbose <server>     Verbose Mode
#            -b <server> | --background <server>  Background Mode
#            -k          | --kill                 Kill
#
# Ex: tunnel -b user@server.com
#     tunnel --verbose user@server.com
#     tunnel -k
#---
function tunnel() {
	local DEFAULT_MODE_OPTION="-vv"

	if  [ -n $(which autossh) ]; then
		local OPTIND
		# Parametros longos
		set -- ${@//--verbose/-v};
		set -- ${@//--background/-b};
		set -- ${@//--kill/-k};

		local VALID_OPT=
		while getopts ":v:b:k" opt ; do
			case $opt in
				k)
					successln "Killing Autossh / Corkscrew";
					_tunnelProxyKill;
					return 0;;
				v)
					_tunnelProxyStart "-vv" "$OPTARG";
					return 0;;
				b)
					_tunnelProxyStart "-f" "$OPTARG";
					return 0;;
				*) usage "${BASH_SOURCE[0]}" tunnel; return 1;;
			esac
			VALID_OPT="OK"
		done
		shift $(($OPTIND - 1))

		if [ -z ${VALID_OPT} ]; then
			usage "${BASH_SOURCE[0]}" tunnel;
			return 1;
		fi

	else
		echo "Autossh is not installed"
		return 1;
	fi;
}

function _tunnelProxyStart() {
	local SSH_OPTIONS="-p 443"
	local DEFAULT_PORT="31282"
	export AUTOSSH_PORT=0

	local prog=autossh;
	ps -Wef | grep -qw "${prog}" && {
		status="running";
	}  || {
		status="starting";
		autossh "$1" -o 'ControlPath none' "${SSH_OPTIONS}" "$2" -L "${DEFAULT_PORT}":localhost:3128 -L 0.0.0.0:"${DEFAULT_PORT}":localhost:3128 -N -oStrictHostKeyChecking=no
	}
	successln "${prog} ${status} for ${2}"
}

function _tunnelProxyKill() {
	tskill /A autossh 2>/dev/null;
	tskill /A corkscrew 2>/dev/null;
}
