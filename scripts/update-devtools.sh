#!/bin/bash
#===================================================================
# ${COLOR_SCRIPT}update-projects.sh${CRESET}
# Checkout or update ${HOME}/projects from repository. Base on projects_list.csv
# Usage: ${COLOR_SCRIPT}update-projects.sh${CRESET}
#        ${COLOR_SCRIPT}update-projects.sh${CRESET} --cleanup
# Aliases: up, update-projects
#
# Author: Luiz Rapatao
# Since: 2014-08-11
#===================================================================
source /opt/devtools/lib/utils.sh
source /opt/devtools/lib/proxy-functions.sh
source /opt/devtools/lib/funcoeszz.sh

# transforma PROJECT_LIST file to array

LOG_FILE=$( logger update-projects.log );

function mainUpdateProjects() {
	
	echo "";
	infoh "Updating Devtools"
	echo "";
	maindir="/opt/"
	for dir in $(ls $maindir --ignore=$BASH_SOURCE)
	do
		if [ -d $maindir$dir/.git ]; then
			info "Updating Devtools"
			cd /opt/$dir
			successln "Devtools $(git pull | zzminusculas)"
		fi
	done;
	notify_info "Update Projects" "Finished"
}

mainUpdateProjects ${@};
unset mainUpdateProjects;